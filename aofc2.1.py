def load_data():
    with open('aofc2.1.input') as f:
        return [x.strip() for x in f.readlines()]


THROW_SCORE = {
    'R': 1,
    'P': 2,
    'S': 3,
}

MAPPING = {
    'A': 'R',
    'B': 'P',
    'C': 'S',
    'X': 'R',
    'Y': 'P',
    'Z': 'S',
}

OUTCOME_SCORE = {
    'R': {
        'R': 3,
        'P': 6,
        'S': 0,
    },
    'P': {
        'R': 0,
        'P': 3,
        'S': 6,
    },
    'S': {
        'R': 6,
        'P': 0,
        'S': 3,
    },
}


def throw_score(throw):
    return THROW_SCORE[MAPPING[throw[2]]]


def outcome_score(play):
    other = MAPPING[play[0]]
    ours = MAPPING[play[2]]

    return OUTCOME_SCORE[other][ours]


def main():
    data = load_data()

    total_score = 0
    for play in data:
        total_score += throw_score(play)
        total_score += outcome_score(play)

    print(total_score)


if __name__ == '__main__':
    main()
