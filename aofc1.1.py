def load_data():
    with open('aofc1.1.input') as f:
        return f.readlines()


def main():
    data = load_data()
    data = [x.strip() for x in data]

    elfs = []
    current_elf = 0
    for item in data:
        if item == '':
            elfs.append(current_elf)
            current_elf = 0
        else:
            current_elf += int(item)
    elfs.append(current_elf)

    print("1.1")
    print(max(elfs))

    elfs.sort()
    print("1.2")
    print(sum(elfs[-3:]))


if __name__ == '__main__':
    main()
