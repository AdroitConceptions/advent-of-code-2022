def load_data():
    with open('aofc2.1.input') as f:
        return [x.strip() for x in f.readlines()]


THROW_SCORE = {
    'R': 1,
    'P': 2,
    'S': 3,
}

MAPPING = {
    'A': 'R',
    'B': 'P',
    'C': 'S',
}

THROW_TO_MAKE = {
    'R': {
        'X': 'S',
        'Y': 'R',
        'Z': 'P',
    },
    'P': {
        'X': 'R',
        'Y': 'P',
        'Z': 'S',
    },
    'S': {
        'X': 'P',
        'Y': 'S',
        'Z': 'R',
    },
}

OUTCOME_SCORE = {
    'R': {
        'R': 3,
        'P': 6,
        'S': 0,
    },
    'P': {
        'R': 0,
        'P': 3,
        'S': 6,
    },
    'S': {
        'R': 6,
        'P': 0,
        'S': 3,
    },
}


def throw_score(throw):
    return THROW_SCORE[throw]


def outcome_score(other, ours):
    return OUTCOME_SCORE[other][ours]


def main():
    data = load_data()

    total_score = 0
    for play in data:
        other = MAPPING[play[0]]
        ours = THROW_TO_MAKE[other][play[2]]

        total_score += throw_score(ours)
        total_score += outcome_score(other, ours)

    print(total_score)


if __name__ == '__main__':
    main()
